<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ngimat');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'quUCE75}~I~ua4UuD;,hXPa8 9}P,yxh&@~wR&!b,Kakv3-S_46@H4@+ztO6MN>G');
define('SECURE_AUTH_KEY',  'O=[5) -We@Q5q=:8d5==NBkhqAca%3{&n%KO}00sEQ;6OY#p?+%?@{kZUvy&oc$)');
define('LOGGED_IN_KEY',    '-ML4*;g2}Ee1oZ6,2Ncnw7n*a`GI]FRd>..CSPmzZJ4!H0;u_a[VQ{9XsuF7BaT~');
define('NONCE_KEY',        ') EmY8!x>Xrp8Y~2B+Ys;Wl.V<!*j:00~${kHwxa7onkK(Olg-Mx];Rk{7mPlUyf');
define('AUTH_SALT',        '3fLd,UtL?uSl}$G h0W.i.L&35l =gvo#5|@4+ERoJ@Pj~z-P!)0L-N0C:E$Tk &');
define('SECURE_AUTH_SALT', 'EV-iv!?yM(-Z(om<PmkOV/$Qcn[KQz *ZjlE|n4KU.ma{5f<EUCZY=|8@vu*:5t|');
define('LOGGED_IN_SALT',   '3Q7<@Ut~cRd1aXh6rn[Hc3OI:.o}se=)RnSNH<JxBkR5xKTPZYCtmr<!x ~831SL');
define('NONCE_SALT',       '4R$==KDZQdJk:Fi>lUr)l 6WIxqxYB?,Jq]y<bsrtw0R~d?CiXj14`/w:G6+|sAy');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ct_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
