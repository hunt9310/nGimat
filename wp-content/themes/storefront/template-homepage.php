<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Homepage
 *
 * @package storefront
 */

get_header(); ?>
	<?php if ( has_post_thumbnail()) : ?>
		<div class="header">
			<div class="inner">
				<div class="top"><?php the_field('top'); ?></div>
				<div class="middle"><?php the_field('middle'); ?></div>
				<hr>
				<div class="bottom"><?php the_field('bottom'); ?></div>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>about-us">Learn More</a>
			</div>
		</div>
		<div id="large" class="image"><?php the_post_thumbnail(); ?></div>
	<?php endif; ?>

	<div class="tag"><?php the_field('tag'); ?></div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="left">
				<?php the_field('left'); ?>
			</div>
			<div class="right">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('posts') ) : 
						endif; ?>
			</div>
			<?php
			/**
			 * Functions hooked in to homepage action
			 *
			 * @hooked storefront_homepage_content      - 10
			 * @hooked storefront_product_categories    - 20
			 * @hooked storefront_recent_products       - 30
			 * @hooked storefront_featured_products     - 40
			 * @hooked storefront_popular_products      - 50
			 * @hooked storefront_on_sale_products      - 60
			 * @hooked storefront_best_selling_products - 70
			 */
			do_action( 'homepage' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
